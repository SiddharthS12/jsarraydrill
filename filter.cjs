const filter = (elements, cb) => {
  const newArray = [];
  if (!Array.isArray(elements)) {
    return newArray;
  }

  if (typeof cb !== "function") {
    return newArray;
  }
  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index], index, elements) === true) {
      newArray.push(elements[index]);
    }
  }
  return newArray;
};

module.exports = filter;
