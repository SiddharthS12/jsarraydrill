const reduce = (elements, cb, startingValue) => {
  const empty = []
  if (!Array.isArray(elements)) {
    return empty
  }

  if (typeof cb !== "function") {
    return empty
  }

  if (elements.length === 0 && startingValue === undefined) {
    return empty
  }

  let accum = 0;
  if (startingValue !== undefined) {
      accum = startingValue
  } else {
    accum = elements[0]
  }

  let index;
  if (startingValue === undefined) {
    index = 1
  } else {
    index = 0
  }

  for (; index < elements.length; index++) {
    accum = cb(accum, elements[index], index, elements);
  }
  return accum;
};

module.exports = reduce;
