const  reduce  = require("../reduce.cjs");

const a = [1, 2, 3, 4, 5, 6];

const sum = reduce(
  a,
  (acc, elements) => {
    return acc + elements;
  },
  0
);

const sumCubes = reduce(a, (acc, elements) => {
  return acc + Math.pow(elements,3)
}, 0)

const generateRandomData = (size, min, max) => {
  const data = [];
  for (let start = 0; start < size; start++) {
    data.push(Math.floor(Math.random() * (max - min + 1)) + min);
  }
  return data;
};

const numbers = generateRandomData(5, 1, 10);
console.log(numbers);
const sumRandomNumbersCube = reduce(numbers, (acc, elements) => {
  return acc + Math.pow(elements,3)
}, 0)

console.log(sum);
console.log(sumCubes);
console.log(sumRandomNumbersCube);
