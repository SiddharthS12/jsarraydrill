const  filter  = require("../filter.cjs");

const a = [10, 20, 33, 45, 50, 63, 70];

const ans = filter(a, (element) => {
    return element % 10 === 0;
})

console.log(ans);