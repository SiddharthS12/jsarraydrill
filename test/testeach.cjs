const  each  = require("../each.cjs");

const a = [1, 2, 3, 4, 5, 5, 5]

each(a, (element, index) => {
    console.log(`Element ${element} is present at index ${index}`);
})