const  flatten  = require("../flatten.cjs");

const a = [1, [2], [[3]], [[[4, 5, 6]]]];

const findDepth = (array) => {
    if (!Array.isArray(array)) {
        return 0;
    }

    let maxDepth = 0;
    for (let index = 0; index < array.length; index++){
        const element = array[index];
        const depth = findDepth(element)
        maxDepth = Math.max(depth, maxDepth);
    }
    return maxDepth + 1;
}

const depth = findDepth(a)
console.log(depth);
const ans = flatten(a,depth);
console.log(ans);
