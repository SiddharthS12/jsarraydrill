const flatten = (elements, depth = 1) => {
  if (depth < 1) {
    const single_d_array = []
    for (let index = 0; index < elements.length; index++){
      single_d_array.push(elements[index])
    }
    return single_d_array
  }
  let flattenedArray = [];

  for (let index = 0; index < elements.length; index++) {
    if (Array.isArray(elements[index])) {
      flattenedArray = flattenedArray.concat(
        flatten(elements[index], depth - 1)
      );
    } else if (elements[index] !== undefined) {
      flattenedArray.push(elements[index]);
    }
  }
  return flattenedArray;
};

module.exports = flatten;
