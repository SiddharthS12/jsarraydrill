const map = (array, cb) => {
  const empty = []
  if (!Array.isArray(array)) {
    return empty
  }

  if (typeof cb !== "function") {
    return empty
  }
  let newArray = [];
  for (let index = 0; index < array.length; index++) {
    // console.log(newArray);

    newArray.push(cb(array[index],index, array));
  }
  return newArray;
};

module.exports = map;
